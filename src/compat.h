#ifndef COMPAT_UTIL_H
#define COMPAT_UTIL_H

#include "../config.h"

/*
 * Platform-specific bits
 */

/* implementations of clock_gettime, clock_nanosleep */

#if !defined (HAVE_CLOCK_NANOSLEEP) && !defined (HAVE_CLOCK_GETTIME)

typedef enum
{
    CLOCK_REALTIME,
    CLOCK_MONOTONIC,
    CLOCK_PROCESS_CPUTIME_ID,
    CLOCK_THREAD_CPUTIME_ID
} clockid_t;
#else
#include <time.h>
#endif // don't have HAVE_CLOCK_NANOSLEEP or HAVE_CLOCK_GETTIME

#ifndef HAVE_CLOCK_GETTIME
    #include <mach/mach_time.h>
    struct timespec;
    int clock_gettime(clockid_t clk_id, struct timespec *tp);
#endif //HAVE_CLOCK_GETTIME


#ifndef HAVE_CLOCK_NANOSLEEP
 
    #ifndef TIMER_ABSTIME
       #define TIMER_ABSTIME 1
    #endif // TIMER_ABSTIME

    struct timespec;

    int clock_nanosleep (clockid_t id, int flags, const struct timespec *ts,
                        struct timespec *ots);
#endif




#ifndef HAVE_ENDIAN_H
    /*
    * Mach endian conversion
    */
    # include <libkern/OSByteOrder.h>
    # define bswap_16 OSSwapInt16
    # define bswap_32 OSSwapInt32
    # define bswap_64 OSSwapInt64
    # include <machine/endian.h>
    # define le16toh(x) OSSwapLittleToHostInt16(x)
    # define le32toh(x) OSSwapLittleToHostInt32(x)

#else // other platforms

    # include <endian.h>

#endif //HAVE_ENDIAN_H



#endif //COMPAT_UTIL_H
