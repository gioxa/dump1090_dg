extern program_global_state_t Modes;

#ifdef HAVE_SDRPLAY
    extern sdrplay_api_DeviceT *chosenDev;
    extern sdrplay_api_CallbackFnsT cbFns;
    extern sdrplay_api_DeviceParamsT *deviceParams;
    extern sdrplay_api_RxChannelParamsT *chParams;
#endif

    extern int masterInitialised, slaveUninitialised;
    extern int slaveAttached;
